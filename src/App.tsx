import React from "react";
import "./App.css";
import ChatPage from "./pages/chat/ChatPage";
import 'antd/dist/antd.css';
import colors from "./common/colors";
import NavBar from "./common/components/navBar/NavBar";
import FootBar from "./common/components/footBar/FootBar";

const  App = (): JSX.Element => (
  <div className={"App"} style={{ backgroundColor: colors.screenBackground}}>
    <NavBar />
        <ChatPage />
    <FootBar />
  </div>
);

export default App;
