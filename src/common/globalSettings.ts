import {useMediaQuery} from "react-responsive";

export const isDesktop = (): boolean => useMediaQuery({
  query: "(min-device-width: 896px)"
});
export const isMobile = (): boolean => useMediaQuery({
  query: "(max-device-width: 896px)"
});
