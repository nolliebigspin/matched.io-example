import React from "react";
import colors from "../../colors";

/**
 * Common component for the blue footer in the bottom
 */
const  FootBar = (): JSX.Element => (
  <div style={{
    marginTop: 30,
    backgroundColor: colors.footerBackground,
    height: 120
  }} />
);

export default FootBar;
