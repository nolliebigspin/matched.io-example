import React, { ReactElement } from "react";
import "./NavBar.css";
import {PageHeader, Button, Avatar, Image} from "antd";
import Logo from "../../../assets/logo.png";
import {isDesktop} from "../../globalSettings";

/**
 * Common component for the navBar in the top of the page
 */
const NavBar = (): ReactElement => {
  const rightButtons: ReactElement =
    <>
      <Button key={1}>Profile</Button>
      <Button key={2}>Matches</Button>
      <Button key={3}>Messages</Button>
      <Avatar key={4} src="https://images.unsplash.com/photo-1558898479-33c0057a5d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80" />
    </>;

  const burger: ReactElement =
    <a className={"burger"}>
      <div className={"bar1"}/>
      <div className={"bar2"}/>
      <div className={"bar3"}/>
    </a>;

  return(
    <PageHeader
      ghost={false}
      title={<Image src={Logo} width={100} preview={false}/>}
      extra={isDesktop() ? rightButtons : burger}
    >
    </PageHeader>
  )
};

export default NavBar;
