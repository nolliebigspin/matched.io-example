import React from "react";
import { IoIosArrowBack } from "react-icons/io";
import colors from "../../colors";
import { Button } from "antd";

interface Props {
  onClick?: () => void;
}

/**
 * Component for the mobile versions back-button to get back to the chatList
 */
const  BackButton: React.FC<Props> = ({onClick}) => (
  <>
    <Button
      type="primary"
      shape="circle"
      icon={<IoIosArrowBack size={20} />}
      onClick={onClick}
      style={{
        background: colors.secondary,
        paddingTop: 5,
        paddingLeft: 3,
        position: "absolute",
        display: "flex"}}
    />
  </>
);

export default BackButton;
