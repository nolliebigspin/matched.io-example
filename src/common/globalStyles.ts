export default {
    // Typography
    "fontWeightBold": 700,
    "fontSizeBig": 20,
    "textOverFlow": {
        "text-overflow": "ellipsis",
        "white-space": "nowrap",
        "overflow": "hidden"
    }
}
