export default {
  primary: "#40f3b2",
  primaryDark: "#32c993",
  secondary: "#597af1",

  strongEmphasis: "#444444",
  mediumEmphasis: "#626262",
  lowEmphasis: "#A9A9A9",

  screenBackground: "#F2F2F2",
  footerBackground: "#1d1728",

  chatBubbleGrey: "#DDD",
  chatBubbleBlue: "#b6dbe0",
}
