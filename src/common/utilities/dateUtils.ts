export const dateToTime = (time: Date): string => {
  const hours = time.getHours();
  const minutes = time.getMinutes();
  return hours + ":" + minutes;
};
