import React, {ReactElement, useState} from "react"
import {isDesktop, isMobile} from "../../common/globalSettings";
import {Card, Col, Row} from "antd";
import ChatHeader from "./components/chatHeader/ChatHeader";
import ChatConversationCard from "./components/chatConversationCard/ChatConversationCard";
import ChatMessagesContainer from "./components/chatMessagesContainer/ChatMessagesContainer";
import companies from "../../mocks/companies.json"

/**
 * The ChatPage is the page that contains all components about the chat
 */
const ChatPage = (): ReactElement => {
  const [chatMessagesOpen, setChatMessagesOpen] = useState<boolean>(false);
  const [openChatId, setOpenChatId] = useState<number>(0);

  const openMessages = (id: number): void => {
    if (!chatMessagesOpen) {
      setChatMessagesOpen(!chatMessagesOpen);
    }
    //Gets correct content from the mockDate
    setOpenChatId(companies.findIndex((i) => i.id === id));
  };

  const closeMessages = (): void => {
    setChatMessagesOpen(false);
  };


  // Creates the list for all ChatConversations
  const chatList: ReactElement  = (
    <>
      {companies.map(c =>
        <ChatConversationCard
          title={c.name}
          jobDescription={c.offer}
          imageUrl={c.image}
          key={c.id}
          onClick={(): void => openMessages(c.id)}
          lastMessage={c.messages[c.messages.length - 1].content}
        />
      )}
    </>
  );

  return (
    <div style={{minHeight: "110vh"}}>
      {isDesktop() &&
        <Row>
          <Col span={chatMessagesOpen ? 12 : 24}>
            {chatList}
          </Col>
          {chatMessagesOpen &&
            <Col span={12}>
              <Card bodyStyle={{ padding: "0" }}>
                <ChatHeader
                  title={companies[openChatId].name}
                  imageUrl={companies[openChatId].image}
                />
                <ChatMessagesContainer chatMessages={companies[openChatId].messages} />
              </Card>
            </Col>
          }
        </Row>
      }
      {isMobile() &&
        <>
        {!chatMessagesOpen ? chatList : (
          <Card bodyStyle={{ padding: "0" }}>
            <ChatHeader title={companies[openChatId].name} imageUrl={companies[openChatId].image}/>
            <ChatMessagesContainer chatMessages={companies[openChatId].messages} backButton goBack={closeMessages}/>
          </Card>
          )}
        </>
      }
    </div>
  )
};

export default ChatPage;
