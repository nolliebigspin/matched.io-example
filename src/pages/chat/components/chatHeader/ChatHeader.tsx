import React, {ReactElement} from "react";
import {Avatar, Button, Card, Col, Row} from "antd";
import globalStyles from "../../../../common/globalStyles";
import colors from "../../../../common/colors";

interface Props {
  title: string;
  imageUrl?: string;
}

/**
 * FC for the header in an open chat
 */
const  ChatHeader: React.FC<Props> = ({
  title,
  imageUrl,
}): ReactElement => {
  if (!imageUrl) {
    imageUrl="http://pronksiapartments.ee/wp-content/uploads/2015/10/placeholder-face-big.png"
  }
  return (
    <Card
      bodyStyle={{padding: "0"}}
      style={{
        backgroundColor: "#FFFFFF",
        padding: 5
      }}>
      <Row align={"middle"}>
        <Col style={{marginInline: 10}}>
          <Avatar size={60} src={imageUrl} />
        </Col>
        <Col>
          <Row justify={"start"}>
            <Col>
              <p style={{
                fontWeight: globalStyles.fontWeightBold,
                fontSize:globalStyles.fontSizeBig
              }}
              >{title}</p>
            </Col>
          </Row>
          <Row justify={"start"}>
            <Button key={1}>Company profile</Button>
            <Button key={2}>Job details</Button>
            <Button key={3} style={{background: colors.primaryDark, marginLeft: "2vw"}}>
              I was hired
            </Button>
          </Row>
        </Col>
      </Row>
    </Card>
  );
};

export default ChatHeader;
