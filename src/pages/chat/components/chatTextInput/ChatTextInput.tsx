import React, { useState } from "react";
import {Button, Col, Row} from "antd";
import colors from "../../../../common/colors";
import { IoIosSend } from 'react-icons/io';
import TextArea from "antd/es/input/TextArea";

interface Props {
  submitMessage: (message: string) => void;
}

/**
 * Component for the textInput and the submit button
 */
const ChatTextInput: React.FC<Props> = ({submitMessage}) => {
  const [input, setInput] = useState<string>("");

  // triggers submit callback
  const handleSubmit = (): void => {
    if (input === "") return;
    submitMessage(input);
    setInput("");
  };

  return (
    <Row align={"middle"} style={{marginTop: 10}}>
      <Col span={18}>
        <TextArea
          onChange={(event) => setInput(event.target.value)}
          value={input}
          placeholder={"Type a Message"}
        />
      </Col>
      <Col span={3}>
        <Row justify={"center"}>
          <Button
            type="primary"
            shape="circle"
            icon={<IoIosSend />}
            style={{ background: colors.secondary }}
            onClick={handleSubmit}
          />
        </Row>
      </Col>
    </Row>
  )
};

export default ChatTextInput;
