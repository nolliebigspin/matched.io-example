import React from "react";
import 'antd/dist/antd.css';
import colors from "../../../../common/colors";
import { Row } from "antd";
interface Props {
  sender: string;
  content: string;
  timeStamp: string;
}

/**
 * Component for the chatBubble itself
 */
const ChatBubble: React.FC<Props> = ({
  sender,
  content,
  timeStamp
}) => (
    <>
      <div style={{
        padding: 15,
        width: "auto",
        height: "auto",
        backgroundColor: sender === "me" ? colors.chatBubbleBlue : colors.chatBubbleGrey,
        borderRadius: sender === "me" ? "20px 20px 0px 20px" : "20px 20px 20px 0px",
        justifyContent: sender === "me" ? "flex-end" : "flex-start",
        marginLeft: sender === "me" ? "5vw" : 0,
        marginRight: sender === "me" ? 0 : "5vw",
      }}>
        <Row>
          {sender !== "me" &&
          <text style={{
            fontSize: 16,
            textAlign: "left",
            color: colors.mediumEmphasis,
            marginBottom: 5
          }}>{sender}</text>}
          <text style={{textAlign: "left"}}>{content}</text>
        </Row>
      </div>
      <div style={{display: "flex",
        justifyContent: sender === "me" ? "flex-end" : "flex-start"
      }}>
        <p>{timeStamp}</p>
      </div>
    </>
  );

export default ChatBubble;
