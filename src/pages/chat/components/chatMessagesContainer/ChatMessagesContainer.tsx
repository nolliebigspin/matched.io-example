import React, {ReactElement, useLayoutEffect, useState} from "react";
import 'antd/dist/antd.css';
import {Card, Col} from "antd";
import BackButton from "../../../../common/components/backButton/BackButton";
import ChatBubble from "./ChatBubble";
import ChatTextInput from "../chatTextInput/ChatTextInput";
import { dateToTime } from "../../../../common/utilities/dateUtils";

interface ChatMessageType {
  sender: string;
  time: string;
  content: string;
}

interface Props {
  chatMessages: ChatMessageType[];
  backButton?: boolean;
  goBack?: () => void;
}

/**
 * Container for that includes the chatBubbles with all messages of a singe chat
 */
const ChatMessagesContainer: React.FC<Props> = ({
  chatMessages,
  backButton,
  goBack,
}) => {
  const [chatContent, setChatContent] = useState<ChatMessageType[]>(chatMessages);

  // this is required to update the actual chat content
  useLayoutEffect(() => {
    setChatContent(chatMessages);
  },[chatMessages]);

  // Creates the element for the chatBubbles in a single chat
  const chatBubbles: ReactElement  = (
    <>
      {chatContent?.map(c =>
        <ChatBubble
          sender={c.sender}
          content={c.content}
          timeStamp={c.time}
        />
      )}
    </>
  );

  // handles submitting new message
  const handleSubmit = (content: string): void => {
    const newMessage: ChatMessageType = {
      sender: "me",
      time: dateToTime(new Date()),
      content: content,
    };
    setChatContent([...chatContent, newMessage])
  };

  return (
    <Card style={{ minHeight: "70vh" }}>
      <Col span={24}>
        {backButton && <BackButton onClick={goBack} />}
        <div style={{ height: "70vh", overflowY: "scroll"}}>
          {chatBubbles}
        </div>
      </Col>
      <ChatTextInput submitMessage={(message): void => handleSubmit(message)}/>
    </Card>
  );
};

export default ChatMessagesContainer;
