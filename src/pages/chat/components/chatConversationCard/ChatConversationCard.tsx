import React, {ReactElement, useState} from "react";
import {Avatar, Card, Col, Row} from "antd";
import colors from "../../../../common/colors";
import globalStyles from "../../../../common/globalStyles";

interface Props {
  title: string;
  jobDescription: string;
  lastMessage: string;
  imageUrl?: string;
  key?: number;
  onClick?: () => void;
}

/**
 * FC for an element of the chat-list
 */
const  ChatConversationCard: React.FC<Props> = ({
  title,
  jobDescription,
  imageUrl,
  onClick,
  lastMessage,
}): ReactElement => {

  const [lastMessageState, setLastMessageState] = useState<string>(lastMessage);

  // shortens the last message that is previewed
  if (lastMessageState?.length > 50) {
    setLastMessageState(lastMessageState.slice(0, 45) + "...")
  }

  // default image if a company doesn't have a profile image
  if (!imageUrl) {
    imageUrl="http://pronksiapartments.ee/wp-content/uploads/2015/10/placeholder-face-big.png"
  }

  return (
    <Card
      bodyStyle={{padding: "0"}}
      style={{
        backgroundColor: "#FFFFFF",
        padding: 14,
        maxHeight: 100,
      }}
      hoverable
      onClick={onClick}>
      <Row align={"middle"}>
        <Col style={{marginInline: 10}}>
          <Avatar size={60} src={imageUrl} />
        </Col>
        <Col>
          <Row justify={"start"}>
            <Col>
              <Row><text>{title}</text></Row>
              <Row>
                <text style={{
                  color: colors.primaryDark,
                  ...globalStyles.textOverFlow
                }}>
                  For job: {jobDescription}
                </text>
              </Row>
              <Row>
                <text
                  style={{color: colors.lowEmphasis}}
                >{lastMessageState ? lastMessageState : "Take the initiative and send a message!"}</text>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    </Card>
  );
};

export default ChatConversationCard;
